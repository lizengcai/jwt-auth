<?php
namespace Anker1992\JWTAuth\Component;

class Tool 
{
    /**
     * 生成随机字符串
     * @param $length 字符串长度
     * @return string
     */
    public static function random($length = 16)
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $str = "";
        for ($i = 0; $i < $length; $i++) {
            $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        }
        return  $str;
    }

    /**
     * 将对象转换成数组
     * @param $obj
     * @return array
     */
    public static function object_to_array($obj)
    {
        $_arr = is_object($obj) ? get_object_vars($obj) : $obj;
        foreach($_arr as $key => $val) {
            $val = (is_array($val) || is_object($val)) ? self::object_to_array($val) : $val;
            $arr[$key] = $val;
        }
        return $arr;
    }
}