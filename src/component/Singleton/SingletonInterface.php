<?php
namespace Anker1992\JWTAuth\Component\Singleton;

interface SingletonInterface {
    public static function getObj();
}