<?php
namespace Anker1992\JWTAuth\Component\Singleton;

trait Singleton {
    static $_instance;

    static function getInstance() {
        if (is_null(self::$_instance)) {
            self::$_instance = static::getObj();
        }
        return self::$_instance;
    }

    static function __callStatic($method, $_param_arr) {
        self::getInstance();
        return call_user_func_array([self::$_instance, $method], $_param_arr);
    }
}