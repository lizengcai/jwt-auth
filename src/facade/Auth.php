<?php
namespace Anker1992\JWTAuth\Facade;

use Anker1992\JWTAuth\Component\Singleton\SingletonInterface;
use Anker1992\JWTAuth\Component\Auth as AuthCpt;
use Anker1992\JWTAuth\Component\Singleton\Singleton;

class Auth implements SingletonInterface {
    use Singleton;

    public static function getObj() {
        //配置 
        $config = [
            'jwtSecret' => 'anker1992', //秘钥
            'userlimit' => 3600, //token有效期
            'refreshLimit' => 3600*24*7 //refresh有效期
        ];
        return new AuthCpt($config);
    }
}