<?php
namespace Anker1992\JWTAuth;

use \Exception;

class JWTAuthException extends Exception {
    protected $message = 'An error occured';
}