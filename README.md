# jwt-auth
jwt认证组件

## 安装
使用composer管理依赖安装方式安装
```
composer require anker1992/jwt-auth
```

## 配置
```
在源代码\Anker1992\JWTAuth\Facade\Auth中修改配置
$config = [
    'jwtSecret' => 'anker1992', //秘钥
    'userlimit' => 3600, //token有效期
    'refreshLimit' => 3600*24*7, //refresh有效期
    'redisAddr' =>  '127.0.0.1', //redis地址
    'redisPort' => 6379 //redis端口
];
```

## 使用案例
项目index.php文件中有示例代码
